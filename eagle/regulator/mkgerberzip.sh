#!/bin/sh
mv -f regulator.GBL \
      regulator.GBO \
      regulator.GBP \
      regulator.GBS \
      regulator.GML \
      regulator.GTL \
      regulator.GTO \
      regulator.GTP \
      regulator.GTS \
      regulator.TXT \
      gerbers
rm regulator.gpi regulator.dri
rm -f regulator-gerbers.zip
zip -r regulator-gerbers.zip gerbers/
