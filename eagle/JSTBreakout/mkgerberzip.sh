#!/bin/sh
mv -f jst_breakout.GBL \
      jst_breakout.GBO \
      jst_breakout.GBP \
      jst_breakout.GBS \
      jst_breakout.GML \
      jst_breakout.GTL \
      jst_breakout.GTO \
      jst_breakout.GTP \
      jst_breakout.GTS \
      jst_breakout.TXT \
      gerbers
rm   jst_breakout.gpi jst_breakout.dri
rm -f jst-gerbers.zip
zip -r jst-gerbers.zip gerbers/
