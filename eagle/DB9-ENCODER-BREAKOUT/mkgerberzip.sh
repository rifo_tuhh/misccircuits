#!/bin/sh
mv -f db9_encoder.GBL \
      db9_encoder.GBO \
      db9_encoder.GBP \
      db9_encoder.GBS \
      db9_encoder.GML \
      db9_encoder.GTL \
      db9_encoder.GTO \
      db9_encoder.GTP \
      db9_encoder.GTS \
      db9_encoder.TXT \
      gerbers
rm    db9_encoder.gpi db9_encoder.dri
rm -f db9-encoder-gerbers.zip
zip -r db9-encoder-gerbers.zip gerbers/
