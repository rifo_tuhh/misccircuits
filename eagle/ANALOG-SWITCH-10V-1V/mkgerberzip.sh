#!/bin/sh
mv -f analog-switch.GBL \
      analog-switch.GBO \
      analog-switch.GBP \
      analog-switch.GBS \
      analog-switch.GML \
      analog-switch.gpi \
      analog-switch.dri \
      analog-switch.GTL \
      analog-switch.GTO \
      analog-switch.GTP \
      analog-switch.GTS \
      analog-switch.TXT \
      gerbers
rm    analog-switch.gpi analog-switch.dri
rm -f analog-switch-gerbers.zip
zip -r analog-switch-gerbers.zip gerbers/
