#!/bin/sh
mv -f Dsub15_breakout.GBL \
      Dsub15_breakout.GBO \
      Dsub15_breakout.GBP \
      Dsub15_breakout.GBS \
      Dsub15_breakout.GML \
      Dsub15_breakout.GTL \
      Dsub15_breakout.GTO \
      Dsub15_breakout.GTP \
      Dsub15_breakout.GTS \
      Dsub15_breakout.TXT \
      gerbers
rm    Dsub15_breakout.gpi Dsub15_breakout.dri
rm -f dsub15-gerbers.zip
zip -r dsub15-gerbers.zip gerbers/
