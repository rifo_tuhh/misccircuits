#!/bin/sh
mv -f darlington.GBL \
      darlington.GBO \
      darlington.GBP \
      darlington.GBS \
      darlington.GML \
      darlington.GTL \
      darlington.GTO \
      darlington.GTP \
      darlington.GTS \
      darlington.TXT \
      gerbers
rm    darlington.gpi darlington.dri
rm -f darlington-gerbers.zip
zip -r darlington-gerbers.zip gerbers/
