#!/bin/sh
mv -f optoTTL2PLC_HF.GBL \
      optoTTL2PLC_HF.GBO \
      optoTTL2PLC_HF.GBP \
      optoTTL2PLC_HF.GBS \
      optoTTL2PLC_HF.GML \
      optoTTL2PLC_HF.GTL \
      optoTTL2PLC_HF.GTO \
      optoTTL2PLC_HF.GTP \
      optoTTL2PLC_HF.GTS \
      optoTTL2PLC_HF.TXT \
      gerbers
rm optoTTL2PLC_HF.gpi optoTTL2PLC_HF.dri
rm -f optoTTL2PLC-gerbers.zip
zip -r optoTTL2PLC-gerbers.zip gerbers/
