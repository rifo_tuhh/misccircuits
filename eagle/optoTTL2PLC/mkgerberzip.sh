#!/bin/sh
mv -f optoTTL2PLC.GBL \
      optoTTL2PLC.GBO \
      optoTTL2PLC.GBP \
      optoTTL2PLC.GBS \
      optoTTL2PLC.GML \
      optoTTL2PLC.GTL \
      optoTTL2PLC.GTO \
      optoTTL2PLC.GTP \
      optoTTL2PLC.GTS \
      optoTTL2PLC.TXT \
      gerbers
rm    optoTTL2PLC.gpi optoTTL2PLC.dri
rm -f optoTTL2PLC-gerbers.zip
zip -r optoTTL2PLC-gerbers.zip gerbers/
