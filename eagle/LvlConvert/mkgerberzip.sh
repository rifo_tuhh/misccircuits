#!/bin/sh
mv -f LvlConvert.GBL \
      LvlConvert.GBO \
      LvlConvert.GBP \
      LvlConvert.GBS \
      LvlConvert.GML \
      LvlConvert.GTL \
      LvlConvert.GTO \
      LvlConvert.GTP \
      LvlConvert.GTS \
      LvlConvert.TXT \
      gerbers
rm    LvlConvert.gpi LvlConvert.dri
rm -f lvlConvert-gerbers.zip
zip -r lvlConvert-gerbers.zip gerbers/
