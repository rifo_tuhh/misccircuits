<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.4">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Image" color="15" fill="10" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="128bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="129" name="129bmp" color="2" fill="4" visible="yes" active="yes"/>
<layer number="130" name="130bmp" color="2" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="trigger-simple">
<packages>
<package name="SOIC24">
<smd name="18" x="0.635" y="4.7" dx="0.6" dy="2" layer="1"/>
<smd name="17" x="1.905" y="4.7" dx="0.6" dy="2" layer="1"/>
<smd name="16" x="3.175" y="4.7" dx="0.6" dy="2" layer="1"/>
<smd name="15" x="4.445" y="4.7" dx="0.6" dy="2" layer="1"/>
<smd name="14" x="5.715" y="4.7" dx="0.6" dy="2" layer="1"/>
<smd name="13" x="6.985" y="4.7" dx="0.6" dy="2" layer="1"/>
<smd name="19" x="-0.635" y="4.7" dx="0.6" dy="2" layer="1"/>
<smd name="20" x="-1.905" y="4.7" dx="0.6" dy="2" layer="1"/>
<smd name="21" x="-3.175" y="4.7" dx="0.6" dy="2" layer="1"/>
<smd name="22" x="-4.445" y="4.7" dx="0.6" dy="2" layer="1"/>
<smd name="23" x="-5.715" y="4.7" dx="0.6" dy="2" layer="1"/>
<smd name="24" x="-6.985" y="4.7" dx="0.6" dy="2" layer="1"/>
<smd name="7" x="0.635" y="-4.7" dx="0.6" dy="2" layer="1"/>
<smd name="8" x="1.905" y="-4.7" dx="0.6" dy="2" layer="1"/>
<smd name="9" x="3.175" y="-4.7" dx="0.6" dy="2" layer="1"/>
<smd name="10" x="4.445" y="-4.7" dx="0.6" dy="2" layer="1"/>
<smd name="11" x="5.715" y="-4.7" dx="0.6" dy="2" layer="1"/>
<smd name="12" x="6.985" y="-4.7" dx="0.6" dy="2" layer="1"/>
<smd name="6" x="-0.635" y="-4.7" dx="0.6" dy="2" layer="1"/>
<smd name="5" x="-1.905" y="-4.7" dx="0.6" dy="2" layer="1"/>
<smd name="4" x="-3.175" y="-4.7" dx="0.6" dy="2" layer="1"/>
<smd name="3" x="-4.445" y="-4.7" dx="0.6" dy="2" layer="1"/>
<smd name="2" x="-5.715" y="-4.7" dx="0.6" dy="2" layer="1"/>
<smd name="1" x="-6.985" y="-4.7" dx="0.6" dy="2" layer="1"/>
<wire x1="-7.8" y1="3.8" x2="7.8" y2="3.8" width="0.127" layer="51"/>
<wire x1="7.8" y1="3.8" x2="7.8" y2="-3.8" width="0.127" layer="51"/>
<wire x1="7.8" y1="-3.8" x2="-7.8" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-7.8" y1="-3.8" x2="-7.8" y2="3.8" width="0.127" layer="51"/>
<wire x1="-7.8" y1="3.5" x2="7.8" y2="3.5" width="0.127" layer="21"/>
<wire x1="7.8" y1="3.5" x2="7.8" y2="-3.5" width="0.127" layer="21"/>
<wire x1="7.8" y1="-3.5" x2="-7.8" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-7.8" y1="-3.5" x2="-7.8" y2="3.5" width="0.127" layer="21"/>
<circle x="-6.95" y="-2.5" radius="0.180275" width="0.127" layer="21"/>
<text x="-8.6" y="-3" size="1.27" layer="25" ratio="10" rot="R90">&gt;Name</text>
<text x="0.85" y="-2.6" size="1.27" layer="27" ratio="10">&gt;Value</text>
</package>
<package name="CAP-PTH-SMALL">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="0.508" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="0.254" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-SMALL2">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-LARGE">
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-4.826" y="0" drill="0.9" diameter="1.905"/>
<pad name="2" x="4.572" y="0" drill="0.9" diameter="1.905"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="GRM43D">
<wire x1="2.25" y1="1.6" x2="1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.6" x2="-2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.6" x2="2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.3" y1="1.8" x2="2.3" y2="1.8" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.8" x2="2.3" y2="-1.8" width="0.127" layer="21"/>
<smd name="A" x="1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<smd name="C" x="-1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<text x="-2" y="2" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-2" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.2" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="2.2" y2="1.6" layer="51"/>
</package>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.5588" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-5MM">
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.5" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="AXIAL-5MM">
<wire x1="-1.14" y1="0.762" x2="1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0.762" x2="1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="-0.762" x2="-1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="-0.762" x2="-1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.394" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.394" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.5" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.5" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.17" size="0.4" layer="25">&gt;Name</text>
<text x="-1.032" y="-0.208" size="0.4" layer="21" ratio="15">&gt;Value</text>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-0.8" y="0.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.7" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="CTZ3">
<description>CTZ3 Series land pattern for variable capacitor - CTZ3E-50C-W1-PF</description>
<wire x1="-1.6" y1="1.4" x2="-1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-2.25" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.4" x2="-1" y2="2.2" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.2" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="51"/>
<wire x1="-1.05" y1="2.25" x2="-1.7" y2="1.45" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.45" x2="-1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2.35" x2="-1.05" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.05" y1="2.25" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.7" y1="-2.35" x2="1.05" y2="-2.35" width="0.127" layer="21"/>
<smd name="+" x="0" y="2.05" dx="1.5" dy="1.2" layer="1"/>
<smd name="-" x="0" y="-2.05" dx="1.5" dy="1.2" layer="1"/>
<text x="-2" y="3" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2" y="-3.4" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="CAP-PTH-SMALLEZ">
<description>This is the "EZ" version of the .1" spaced ceramic thru-hole cap.&lt;br&gt;
It has reduced top mask to make it harder to put the component on the wrong side of the board.</description>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651" stop="no"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651" stop="no"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
<circle x="0" y="0" radius="0.898025" width="0" layer="30"/>
<circle x="2.54" y="0" radius="0.915809375" width="0" layer="30"/>
<circle x="0" y="0" radius="0.40160625" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.40160625" width="0" layer="29"/>
</package>
<package name="SCREWTERMINAL-2.5MM-8">
<pad name="8" x="0" y="0" drill="1" shape="square"/>
<pad name="6" x="5" y="0" drill="1" shape="square"/>
<pad name="4" x="10" y="0" drill="1" shape="square"/>
<pad name="2" x="15" y="0" drill="1" shape="square"/>
<pad name="7" x="2.5" y="-5" drill="1" shape="square"/>
<pad name="5" x="7.5" y="-5" drill="1" shape="square"/>
<pad name="3" x="12.5" y="-5" drill="1" shape="square"/>
<pad name="1" x="17.5" y="-5" drill="1" shape="square"/>
<wire x1="-2.75" y1="2.3" x2="18.75" y2="2.3" width="0.127" layer="51"/>
<wire x1="18.75" y1="2.3" x2="18.75" y2="-9.7" width="0.127" layer="51"/>
<wire x1="18.75" y1="-9.7" x2="-2.75" y2="-9.7" width="0.127" layer="51"/>
<wire x1="-2.75" y1="-9.7" x2="-2.75" y2="2.3" width="0.127" layer="51"/>
<wire x1="-3" y1="2.55" x2="19" y2="2.55" width="0.127" layer="21"/>
<wire x1="19" y1="2.55" x2="19" y2="-10" width="0.127" layer="21"/>
<wire x1="19" y1="-10" x2="-3" y2="-10" width="0.127" layer="21"/>
<wire x1="-3" y1="-10" x2="-3" y2="2.55" width="0.127" layer="21"/>
<text x="17.3" y="-3.15" size="1.27" layer="21">1</text>
<text x="14.65" y="-3.15" size="1.27" layer="21">2</text>
<text x="12.2" y="-3.15" size="1.27" layer="21">3</text>
<text x="9.65" y="-3.15" size="1.27" layer="21">4</text>
<text x="7.15" y="-3.15" size="1.27" layer="21">5</text>
<text x="4.6" y="-3.15" size="1.27" layer="21">6</text>
<text x="2.3" y="-3.15" size="1.27" layer="21">7</text>
<text x="-0.5" y="-3.15" size="1.27" layer="21">8</text>
<text x="-3.7" y="-9.4" size="1.27" layer="25" rot="R90">&gt;Name</text>
</package>
<package name="SCREWTERMINAL-2.5MM-2">
<pad name="2" x="0" y="0" drill="1" shape="square"/>
<pad name="1" x="2.5" y="-5" drill="1" shape="square"/>
<wire x1="-3" y1="2.55" x2="4.2" y2="2.55" width="0.127" layer="21"/>
<wire x1="4.2" y1="2.55" x2="4.2" y2="-10" width="0.127" layer="21"/>
<wire x1="4.2" y1="-10" x2="-3" y2="-10" width="0.127" layer="21"/>
<wire x1="-3" y1="-10" x2="-3" y2="2.55" width="0.127" layer="21"/>
<wire x1="-2.75" y1="2.3" x2="3.75" y2="2.3" width="0.127" layer="51"/>
<wire x1="3.75" y1="2.3" x2="3.75" y2="-9.7" width="0.127" layer="51"/>
<wire x1="3.75" y1="-9.7" x2="-2.75" y2="-9.7" width="0.127" layer="51"/>
<wire x1="-2.75" y1="-9.7" x2="-2.75" y2="2.3" width="0.127" layer="51"/>
<text x="2.2" y="-3.25" size="1.27" layer="21">1</text>
<text x="-0.45" y="-3.3" size="1.27" layer="21">2</text>
<text x="-3.95" y="-9.5" size="1.27" layer="25" rot="R90">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="SN74LVC4245A">
<pin name="VCCA" x="-7.62" y="15.24" length="middle"/>
<pin name="DIR" x="-7.62" y="12.7" length="middle"/>
<pin name="A1" x="-7.62" y="10.16" length="middle"/>
<pin name="A3" x="-7.62" y="5.08" length="middle"/>
<pin name="A2" x="-7.62" y="7.62" length="middle"/>
<pin name="A4" x="-7.62" y="2.54" length="middle"/>
<pin name="A5" x="-7.62" y="0" length="middle"/>
<pin name="A6" x="-7.62" y="-2.54" length="middle"/>
<pin name="A7" x="-7.62" y="-5.08" length="middle"/>
<pin name="A8" x="-7.62" y="-7.62" length="middle"/>
<pin name="GND@11" x="-7.62" y="-10.16" length="middle"/>
<pin name="GND@12" x="-7.62" y="-12.7" length="middle"/>
<pin name="VCCB@24" x="25.4" y="15.24" length="middle" rot="R180"/>
<pin name="VCCB@23" x="25.4" y="12.7" length="middle" rot="R180"/>
<pin name="OE_N" x="25.4" y="10.16" length="middle" rot="R180"/>
<pin name="B1" x="25.4" y="7.62" length="middle" rot="R180"/>
<pin name="B2" x="25.4" y="5.08" length="middle" rot="R180"/>
<pin name="B3" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="B4" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="B5" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="B6" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="B7" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="B8" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="GND@13" x="25.4" y="-12.7" length="middle" rot="R180"/>
<wire x1="-2.54" y1="17.78" x2="-2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="20.32" y2="-15.24" width="0.254" layer="94"/>
<wire x1="20.32" y1="-15.24" x2="20.32" y2="17.78" width="0.254" layer="94"/>
<wire x1="20.32" y1="17.78" x2="-2.54" y2="17.78" width="0.254" layer="94"/>
<text x="-2.54" y="-17.78" size="2.032" layer="95" ratio="10">&gt;Name</text>
<text x="-2.54" y="20.32" size="1.778" layer="96">SN74LVC4245A</text>
</symbol>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="5V">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="3.3V">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="CONN-8">
<pin name="8" x="-5.08" y="7.62" length="short"/>
<pin name="7" x="-5.08" y="5.08" length="short"/>
<pin name="6" x="-5.08" y="2.54" length="short"/>
<pin name="5" x="-5.08" y="0" length="short"/>
<pin name="4" x="-5.08" y="-2.54" length="short"/>
<pin name="3" x="-5.08" y="-5.08" length="short"/>
<pin name="2" x="-5.08" y="-7.62" length="short"/>
<pin name="1" x="-5.08" y="-10.16" length="short"/>
<wire x1="-2.54" y1="10.16" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-12.7" x2="2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
<text x="-2.54" y="-15.24" size="1.27" layer="95">&gt;Name</text>
</symbol>
<symbol name="GND_EARTH">
<wire x1="-1.27" y1="-0.635" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="-1.27" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="0" width="0.1524" layer="94"/>
<pin name="E_GND" x="0" y="0" visible="off" length="point" direction="sup" rot="R270"/>
<text x="-2.54" y="-2.54" size="0.8128" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="CONN-2">
<pin name="2" x="-2.54" y="2.54" length="short"/>
<pin name="1" x="-2.54" y="0" length="short"/>
<wire x1="0" y1="5.08" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<text x="0" y="-5.08" size="1.27" layer="95">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SN74LVC4245A" prefix="U" uservalue="yes">
<description>&lt;b&gt;SN74LVC4245ADWR &lt;/b&gt; - IC BUS TRANSCEIVER 8BIT 24SOIC
     
    &lt;p&gt;Technical Specifications:
    &lt;ul&gt;
    &lt;li&gt;VCC: 2.7 - 5.5V&lt;/li&gt;
    &lt;li&gt;Operating Temperature: -40°C to 85°C&lt;/li&gt;
    &lt;li&gt;bidirectional 3 state support&lt;/li&gt;

    &lt;/ul&gt;
    &lt;/p&gt;
     
    &lt;p&gt;Digikey: 296-14911-1-ND &lt;br/&gt;
    &lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SN74LVC4245A" x="-7.62" y="-2.54"/>
</gates>
<devices>
<device name="" package="SOIC24">
<connects>
<connect gate="G$1" pin="A1" pad="3"/>
<connect gate="G$1" pin="A2" pad="4"/>
<connect gate="G$1" pin="A3" pad="5"/>
<connect gate="G$1" pin="A4" pad="6"/>
<connect gate="G$1" pin="A5" pad="7"/>
<connect gate="G$1" pin="A6" pad="8"/>
<connect gate="G$1" pin="A7" pad="9"/>
<connect gate="G$1" pin="A8" pad="10"/>
<connect gate="G$1" pin="B1" pad="21"/>
<connect gate="G$1" pin="B2" pad="20"/>
<connect gate="G$1" pin="B3" pad="19"/>
<connect gate="G$1" pin="B4" pad="18"/>
<connect gate="G$1" pin="B5" pad="17"/>
<connect gate="G$1" pin="B6" pad="16"/>
<connect gate="G$1" pin="B7" pad="15"/>
<connect gate="G$1" pin="B8" pad="14"/>
<connect gate="G$1" pin="DIR" pad="2"/>
<connect gate="G$1" pin="GND@11" pad="11"/>
<connect gate="G$1" pin="GND@12" pad="12"/>
<connect gate="G$1" pin="GND@13" pad="13"/>
<connect gate="G$1" pin="OE_N" pad="22"/>
<connect gate="G$1" pin="VCCA" pad="1"/>
<connect gate="G$1" pin="VCCB@23" pad="23"/>
<connect gate="G$1" pin="VCCB@24" pad="24"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="CAP-PTH-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CAP-PTH-SMALL2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CAP-PTH-LARGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="GRM43D">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CAP-PTH-5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_" package="AXIAL-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ASMD" package="CTZ3">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="CAP-PTH-SMALLEZ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="5V" prefix="SUPPLY">
<gates>
<gate name="G$1" symbol="5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3.3V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN-8" prefix="J" uservalue="yes">
<description>&lt;b&gt;CONN TERM BLOCK SPRING 8POS 2.5MM&lt;/b&gt; 
&lt;p&gt;
Pitch 2.50 mm&lt;br&gt;
2A - 300V&lt;br&gt;
Wire Gauge 20-26 AWG&lt;br&gt;
Digikey No: 277-1800-ND
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CONN-8" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="SCREWTERMINAL-2.5MM-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND_EARTH" uservalue="yes">
<gates>
<gate name="G$1" symbol="GND_EARTH" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN-2" prefix="J" uservalue="yes">
<description>&lt;b&gt;CONN TERM BLOCK SPRING 2POS 2.5MM&lt;/b&gt; 
&lt;p&gt;
Pitch 2.50 mm&lt;br&gt;
2A - 300V&lt;br&gt;
Wire Gauge 20-26 AWG&lt;br&gt;
Digikey No: 277-1794-ND
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CONN-2" x="-5.08" y="0"/>
</gates>
<devices>
<device name="" package="SCREWTERMINAL-2.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Passives">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762"/>
<pad name="2" x="2.5" y="0" drill="0.762"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;

Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;br&gt;
&lt;br&gt;

&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.159" y="-0.762" size="1.27" layer="21" font="vector" ratio="15">&gt;Value</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="AXIAL-0.3EZ">
<description>This is the "EZ" version of the standard .3" spaced resistor package.&lt;br&gt;
It has a reduced top mask to make it harder to install upside-down.</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="-3.81" y="0" radius="0.508" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.523634375" width="0" layer="29"/>
<circle x="-3.81" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="3.81" y="0" radius="1.04726875" width="0" layer="30"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="AXIAL-0.3EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="holes">
<description>&lt;b&gt;Mounting Holes and Pads&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2,8-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 2.8 mm, round</description>
<wire x1="0" y1="2.921" x2="0" y2="2.667" width="0.0508" layer="21"/>
<wire x1="0" y1="-2.667" x2="0" y2="-2.921" width="0.0508" layer="21"/>
<wire x1="-1.778" y1="0" x2="0" y2="-1.778" width="2.286" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="1.778" x2="1.778" y2="0" width="2.286" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="0.635" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="2.921" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="39"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="40"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="43"/>
<circle x="0" y="0" radius="1.5" width="0.2032" layer="21"/>
<pad name="B2,8" x="0" y="0" drill="2.8" diameter="5.334"/>
</package>
<package name="3,0-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.0 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="39"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.6" width="0.2032" layer="21"/>
<pad name="B3,0" x="0" y="0" drill="3" diameter="5.842"/>
<text x="-1.27" y="-3.81" size="1.27" layer="48">3,0</text>
</package>
<package name="3,2-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.2 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.7" width="0.1524" layer="21"/>
<pad name="B3,2" x="0" y="0" drill="3.2" diameter="5.842"/>
<text x="-1.27" y="-3.81" size="1.27" layer="48">3,2</text>
</package>
<package name="3,3-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.3 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.7" width="0.2032" layer="21"/>
<pad name="B3,3" x="0" y="0" drill="3.3" diameter="5.842"/>
</package>
<package name="3,6-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.6 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.397" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.397" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.9" width="0.2032" layer="21"/>
<pad name="B3,6" x="0" y="0" drill="3.6" diameter="5.842"/>
</package>
<package name="4,1-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.1 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="5.08" width="2" layer="43"/>
<circle x="0" y="0" radius="2.15" width="0.2032" layer="21"/>
<pad name="B4,1" x="0" y="0" drill="4.1" diameter="8"/>
</package>
<package name="4,3-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.3 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.25" width="0.1524" layer="21"/>
<pad name="B4,3" x="0" y="0" drill="4.3" diameter="9"/>
</package>
<package name="4,5-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.5 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.35" width="0.1524" layer="21"/>
<pad name="B4,5" x="0" y="0" drill="4.5" diameter="9"/>
</package>
<package name="5,0-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 5.0 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.6" width="0.1524" layer="21"/>
<pad name="B5" x="0" y="0" drill="5" diameter="9"/>
</package>
<package name="5,5-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 5.5 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.85" width="0.1524" layer="21"/>
<pad name="B5,5" x="0" y="0" drill="5.5" diameter="9"/>
</package>
</packages>
<symbols>
<symbol name="MOUNT-PAD">
<wire x1="0.254" y1="2.032" x2="2.032" y2="0.254" width="1.016" layer="94" curve="-75.749967" cap="flat"/>
<wire x1="-2.032" y1="0.254" x2="-0.254" y2="2.032" width="1.016" layer="94" curve="-75.749967" cap="flat"/>
<wire x1="-2.032" y1="-0.254" x2="-0.254" y2="-2.032" width="1.016" layer="94" curve="75.749967" cap="flat"/>
<wire x1="0.254" y1="-2.032" x2="2.032" y2="-0.254" width="1.016" layer="94" curve="75.749967" cap="flat"/>
<circle x="0" y="0" radius="1.524" width="0.0508" layer="94"/>
<text x="2.794" y="0.5842" size="1.778" layer="95">&gt;NAME</text>
<text x="2.794" y="-2.4638" size="1.778" layer="96">&gt;VALUE</text>
<pin name="MOUNT" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOUNT-PAD-ROUND" prefix="H">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt;, round</description>
<gates>
<gate name="G$1" symbol="MOUNT-PAD" x="0" y="0"/>
</gates>
<devices>
<device name="2.8" package="2,8-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B2,8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.0" package="3,0-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,0"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.2" package="3,2-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.3" package="3,3-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.6" package="3,6-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.1" package="4,1-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.3" package="4,3-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.5" package="4,5-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5.0" package="5,0-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5.5" package="5,5-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B5,5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dan-rcl">
<packages>
<package name="0402">
<description>&lt;b&gt;0402&lt;/b&gt;
&lt;p&gt;From Panasonic ERJ-2RKF1000X Recommended Land Patterns: http://industrial.panasonic.com/www-data/pdf/AOA0000/AOA0000PE32.pdf&lt;/p&gt;
&lt;p&gt;Can fit one 8-mil (0.2032mm) trace between pads&lt;/p&gt;</description>
<smd name="2" x="0.6548" y="0" dx="0.7" dy="0.6" layer="1"/>
<smd name="1" x="-0.6548" y="0" dx="0.7" dy="0.6" layer="1"/>
<text x="1.27" y="-0.635" size="1.016" layer="25" font="vector" ratio="20">&gt;NAME</text>
<rectangle x1="-0.3048" y1="-0.25" x2="0.3048" y2="-0.0468" layer="21"/>
<rectangle x1="-0.525" y1="-0.275" x2="-0.15" y2="0.275" layer="51"/>
<rectangle x1="0.15" y1="-0.275" x2="0.525" y2="0.275" layer="51" rot="R180"/>
<rectangle x1="-0.3048" y1="0.0468" x2="0.3048" y2="0.25" layer="21" rot="R180"/>
</package>
<package name="0603">
<description>&lt;b&gt;0603&lt;/b&gt;
&lt;p&gt;From Panasonic ERJ-2RKF1000X Recommended Land Patterns: http://industrial.panasonic.com/www-data/pdf/AOA0000/AOA0000PE32.pdf&lt;/p&gt;
&lt;p&gt;Can fit two 6.25-mil (0.15875mm) traces between pads (on 0.00625 grid)&lt;/p&gt;</description>
<smd name="2" x="0.725" y="0" dx="0.55" dy="1" layer="1"/>
<smd name="1" x="-0.725" y="0" dx="0.55" dy="1" layer="1"/>
<text x="1.27" y="-0.635" size="1.016" layer="25" font="vector" ratio="20">&gt;NAME</text>
<rectangle x1="-0.45" y1="-0.4" x2="0.45" y2="-0.1968" layer="21"/>
<rectangle x1="-0.825" y1="-0.425" x2="-0.4" y2="0.425" layer="51"/>
<rectangle x1="0.4" y1="-0.425" x2="0.825" y2="0.425" layer="51" rot="R180"/>
<rectangle x1="-0.45" y1="0.1968" x2="0.45" y2="0.4" layer="21" rot="R180"/>
</package>
<package name="0805">
<description>&lt;b&gt;0805&lt;/b&gt;
&lt;p&gt;From Panasonic ERJ-2RKF1000X Recommended Land Patterns: http://industrial.panasonic.com/www-data/pdf/AOA0000/AOA0000PE32.pdf&lt;/p&gt;
&lt;p&gt;Can fit three 6.25-mil (0.15875mm) traces between pads (on 0.00625 grid)&lt;/p&gt;</description>
<smd name="2" x="1.1" y="0" dx="0.9" dy="1.4" layer="1"/>
<smd name="1" x="-1.1" y="0" dx="0.9" dy="1.4" layer="1"/>
<text x="1.905" y="-0.635" size="1.016" layer="25" font="vector" ratio="20">&gt;NAME</text>
<rectangle x1="-0.65" y1="-0.625" x2="0.65" y2="-0.4218" layer="21"/>
<rectangle x1="-1.025" y1="-0.65" x2="-0.475" y2="0.65" layer="51"/>
<rectangle x1="0.475" y1="-0.65" x2="1.025" y2="0.65" layer="51" rot="R180"/>
<rectangle x1="-0.65" y1="0.4218" x2="0.65" y2="0.625" layer="21" rot="R180"/>
</package>
<package name="1206">
<description>&lt;b&gt;1206&lt;/b&gt;
&lt;p&gt;From Panasonic ERJ-2RKF1000X Recommended Land Patterns: http://industrial.panasonic.com/www-data/pdf/AOA0000/AOA0000PE32.pdf&lt;/p&gt;
&lt;p&gt;Can fit six 6.25-mil (0.15875mm) traces between pads (on 0.00625 grid)&lt;/p&gt;</description>
<smd name="1" x="-1.65" y="0" dx="1.1" dy="1.8" layer="1"/>
<smd name="2" x="1.65" y="0" dx="1.1" dy="1.8" layer="1" rot="R180"/>
<text x="3.175" y="-0.635" size="1.016" layer="25" font="vector" ratio="20">&gt;NAME</text>
<rectangle x1="-1.1" y1="-0.8" x2="1.1" y2="-0.5968" layer="21"/>
<rectangle x1="-1.625" y1="-0.825" x2="-1" y2="0.825" layer="51"/>
<rectangle x1="1" y1="-0.825" x2="1.625" y2="0.825" layer="51" rot="R180"/>
<rectangle x1="-1.1" y1="0.5968" x2="1.1" y2="0.8" layer="21" rot="R180"/>
</package>
<package name="1206H">
<description>&lt;b&gt;High-power 1206&lt;/b&gt;
&lt;p&gt;From Panasonic ERJ-2RKF1000X Recommended Land Patterns: http://industrial.panasonic.com/www-data/pdf/AOA0000/AOA0000PE32.pdf&lt;/p&gt;
&lt;p&gt;Can fit three 6.25-mil (0.15875mm) trace between pads (on 0.00625 grid)&lt;/p&gt;</description>
<smd name="1" x="-1.55" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="2" x="1.55" y="0" dx="1.9" dy="1.8" layer="1" rot="R180"/>
<text x="3.175" y="-0.635" size="1.016" layer="25" font="vector" ratio="20">&gt;NAME</text>
<rectangle x1="-1.625" y1="-0.825" x2="-1" y2="0.825" layer="51"/>
<rectangle x1="1" y1="-0.825" x2="1.625" y2="0.825" layer="51" rot="R180"/>
<rectangle x1="-0.6" y1="0.5968" x2="0.6" y2="0.8" layer="21" rot="R180"/>
<rectangle x1="-0.6" y1="-0.8" x2="0.6" y2="-0.5968" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="FERRITE">
<wire x1="0" y1="1.016" x2="0.508" y2="0" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="1.016" y2="-1.016" width="0.254" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.016" x2="2.032" y2="0" width="0.254" layer="94"/>
<wire x1="2.032" y1="0" x2="1.524" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.524" y1="1.016" x2="0" y2="1.016" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.1524" layer="94"/>
<text x="2.794" y="1.524" size="1.27" layer="95" rot="MR180">&gt;NAME</text>
<text x="-0.254" y="1.524" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FERRITE" prefix="FB" uservalue="yes">
<description>&lt;b&gt;Ferrite Bead&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="FERRITE" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206H" package="1206H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="trigger-simple" deviceset="SN74LVC4245A" device=""/>
<part name="GND9" library="trigger-simple" deviceset="GND" device=""/>
<part name="GND10" library="trigger-simple" deviceset="GND" device=""/>
<part name="J4" library="trigger-simple" deviceset="CONN-8" device=""/>
<part name="SUPPLY8" library="trigger-simple" deviceset="3.3V" device=""/>
<part name="J5" library="trigger-simple" deviceset="CONN-8" device=""/>
<part name="SUPPLY9" library="trigger-simple" deviceset="5V" device=""/>
<part name="R9" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R10" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R11" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R12" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R13" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R14" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R15" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R16" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="GND14" library="trigger-simple" deviceset="GND" device=""/>
<part name="C3" library="trigger-simple" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C4" library="trigger-simple" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="SUPPLY10" library="trigger-simple" deviceset="5V" device=""/>
<part name="SUPPLY11" library="trigger-simple" deviceset="3.3V" device=""/>
<part name="GND12" library="trigger-simple" deviceset="GND" device=""/>
<part name="GND13" library="trigger-simple" deviceset="GND" device=""/>
<part name="C7" library="trigger-simple" deviceset="CAP" device="0805" value="1uF"/>
<part name="C8" library="trigger-simple" deviceset="CAP" device="0805" value="1uF"/>
<part name="SUPPLY16" library="trigger-simple" deviceset="5V" device=""/>
<part name="SUPPLY17" library="trigger-simple" deviceset="3.3V" device=""/>
<part name="GND22" library="trigger-simple" deviceset="GND" device=""/>
<part name="GND23" library="trigger-simple" deviceset="GND" device=""/>
<part name="U4" library="trigger-simple" deviceset="SN74LVC4245A" device=""/>
<part name="GND4" library="trigger-simple" deviceset="GND" device=""/>
<part name="GND24" library="trigger-simple" deviceset="GND" device=""/>
<part name="J8" library="trigger-simple" deviceset="CONN-8" device=""/>
<part name="SUPPLY3" library="trigger-simple" deviceset="3.3V" device=""/>
<part name="J9" library="trigger-simple" deviceset="CONN-8" device=""/>
<part name="SUPPLY18" library="trigger-simple" deviceset="5V" device=""/>
<part name="R25" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R26" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R27" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R28" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R29" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R30" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R31" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="R32" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="GND26" library="trigger-simple" deviceset="GND" device=""/>
<part name="GND27" library="trigger-simple" deviceset="GND" device=""/>
<part name="C9" library="trigger-simple" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C10" library="trigger-simple" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="SUPPLY19" library="trigger-simple" deviceset="5V" device=""/>
<part name="SUPPLY20" library="trigger-simple" deviceset="3.3V" device=""/>
<part name="GND28" library="trigger-simple" deviceset="GND" device=""/>
<part name="GND29" library="trigger-simple" deviceset="GND" device=""/>
<part name="H3" library="holes" deviceset="MOUNT-PAD-ROUND" device="3.2" value="M3"/>
<part name="H4" library="holes" deviceset="MOUNT-PAD-ROUND" device="3.2" value="M3"/>
<part name="U$2" library="trigger-simple" deviceset="GND_EARTH" device=""/>
<part name="U$3" library="trigger-simple" deviceset="GND_EARTH" device=""/>
<part name="GND30" library="trigger-simple" deviceset="GND" device=""/>
<part name="FB1" library="dan-rcl" deviceset="FERRITE" device="0805"/>
<part name="R34" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="SUPPLY22" library="trigger-simple" deviceset="5V" device=""/>
<part name="SUPPLY24" library="trigger-simple" deviceset="5V" device=""/>
<part name="SUPPLY26" library="trigger-simple" deviceset="3.3V" device=""/>
<part name="J10" library="trigger-simple" deviceset="CONN-2" device=""/>
<part name="J11" library="trigger-simple" deviceset="CONN-2" device=""/>
<part name="J12" library="trigger-simple" deviceset="CONN-2" device=""/>
<part name="SUPPLY27" library="trigger-simple" deviceset="5V" device=""/>
<part name="SUPPLY28" library="trigger-simple" deviceset="3.3V" device=""/>
<part name="GND5" library="trigger-simple" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="17.78" y="99.06" size="1.778" layer="97">when DIR is high 5V-&gt;3V</text>
<text x="-15.24" y="-35.56" size="1.778" layer="97">when DIR is high 5V-&gt;3V</text>
<wire x1="-71.12" y1="218.44" x2="185.42" y2="218.44" width="0.1524" layer="94"/>
<wire x1="185.42" y1="218.44" x2="185.42" y2="-109.22" width="0.1524" layer="94"/>
<wire x1="185.42" y1="-109.22" x2="-71.12" y2="-109.22" width="0.1524" layer="94"/>
<wire x1="-71.12" y1="-109.22" x2="-71.12" y2="218.44" width="0.1524" layer="94"/>
<text x="-60.96" y="210.82" size="5.08" layer="94" ratio="10">Level Translator Breakout Board</text>
<text x="114.3" y="200.66" size="2.54" layer="97" ratio="10">Watch for power up sequence!!</text>
<text x="114.3" y="205.74" size="2.54" layer="97" ratio="10">NOTES</text>
</plain>
<instances>
<instance part="U1" gate="G$1" x="25.4" y="76.2"/>
<instance part="GND9" gate="1" x="53.34" y="58.42"/>
<instance part="GND10" gate="1" x="15.24" y="58.42"/>
<instance part="J4" gate="G$1" x="-35.56" y="76.2" smashed="yes" rot="R180">
<attribute name="NAME" x="-35.56" y="63.5" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="SUPPLY8" gate="G$1" x="55.88" y="93.98"/>
<instance part="J5" gate="G$1" x="76.2" y="76.2" smashed="yes">
<attribute name="NAME" x="76.2" y="88.9" size="1.27" layer="95"/>
</instance>
<instance part="SUPPLY9" gate="G$1" x="12.7" y="93.98"/>
<instance part="R9" gate="G$1" x="10.16" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="8.6614" y="49.53" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="8.382" y="54.61" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R10" gate="G$1" x="5.08" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="3.5814" y="49.53" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="3.302" y="54.61" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R11" gate="G$1" x="-5.08" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="-6.5786" y="49.53" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-6.858" y="54.61" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R12" gate="G$1" x="-10.16" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="-11.6586" y="49.53" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-11.938" y="54.61" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R13" gate="G$1" x="-15.24" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="-16.7386" y="49.53" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-17.018" y="54.61" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R14" gate="G$1" x="-25.4" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="-26.8986" y="49.53" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-27.178" y="54.61" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R15" gate="G$1" x="-20.32" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="-21.8186" y="49.53" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-22.098" y="54.61" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R16" gate="G$1" x="0" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="-1.4986" y="49.53" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-1.778" y="54.61" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND14" gate="1" x="66.04" y="88.9" rot="R90"/>
<instance part="C3" gate="G$1" x="91.44" y="73.66" smashed="yes">
<attribute name="NAME" x="92.964" y="76.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="88.519" y="67.564" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="99.06" y="73.66" smashed="yes">
<attribute name="NAME" x="100.584" y="76.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="103.759" y="67.564" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY10" gate="G$1" x="91.44" y="81.28"/>
<instance part="SUPPLY11" gate="G$1" x="99.06" y="81.28"/>
<instance part="GND12" gate="1" x="91.44" y="66.04"/>
<instance part="GND13" gate="1" x="99.06" y="66.04"/>
<instance part="C7" gate="G$1" x="-27.94" y="187.96" smashed="yes">
<attribute name="NAME" x="-26.416" y="190.881" size="1.778" layer="95"/>
<attribute name="VALUE" x="-30.861" y="181.864" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C8" gate="G$1" x="-17.78" y="187.96" smashed="yes">
<attribute name="NAME" x="-16.256" y="190.881" size="1.778" layer="95"/>
<attribute name="VALUE" x="-13.081" y="181.864" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY16" gate="G$1" x="-27.94" y="195.58"/>
<instance part="SUPPLY17" gate="G$1" x="-17.78" y="195.58"/>
<instance part="GND22" gate="1" x="-27.94" y="180.34"/>
<instance part="GND23" gate="1" x="-17.78" y="180.34"/>
<instance part="U4" gate="G$1" x="-7.62" y="-58.42"/>
<instance part="GND4" gate="1" x="20.32" y="-76.2"/>
<instance part="GND24" gate="1" x="-17.78" y="-76.2"/>
<instance part="J8" gate="G$1" x="-35.56" y="-58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="-35.56" y="-71.12" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="22.86" y="-40.64"/>
<instance part="J9" gate="G$1" x="81.28" y="-58.42" smashed="yes">
<attribute name="NAME" x="81.28" y="-45.72" size="1.27" layer="95"/>
</instance>
<instance part="SUPPLY18" gate="G$1" x="-20.32" y="-40.64"/>
<instance part="R25" gate="G$1" x="68.58" y="-81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="67.0814" y="-85.09" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="66.802" y="-80.01" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R26" gate="G$1" x="63.5" y="-81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="62.0014" y="-85.09" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="61.722" y="-80.01" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R27" gate="G$1" x="53.34" y="-81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="51.8414" y="-85.09" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="51.562" y="-80.01" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R28" gate="G$1" x="48.26" y="-81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="46.7614" y="-85.09" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="46.482" y="-80.01" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R29" gate="G$1" x="43.18" y="-81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="41.6814" y="-85.09" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="41.402" y="-80.01" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R30" gate="G$1" x="33.02" y="-81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="31.5214" y="-85.09" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="31.242" y="-80.01" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R31" gate="G$1" x="38.1" y="-81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="36.6014" y="-85.09" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="36.322" y="-80.01" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R32" gate="G$1" x="58.42" y="-81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="56.9214" y="-85.09" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="56.642" y="-80.01" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND26" gate="1" x="33.02" y="-45.72" rot="R90"/>
<instance part="GND27" gate="1" x="-27.94" y="-43.18" rot="R270"/>
<instance part="C9" gate="G$1" x="93.98" y="-60.96" smashed="yes">
<attribute name="NAME" x="95.504" y="-58.039" size="1.778" layer="95"/>
<attribute name="VALUE" x="91.059" y="-67.056" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C10" gate="G$1" x="101.6" y="-60.96" smashed="yes">
<attribute name="NAME" x="103.124" y="-58.039" size="1.778" layer="95"/>
<attribute name="VALUE" x="106.299" y="-67.056" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY19" gate="G$1" x="93.98" y="-53.34"/>
<instance part="SUPPLY20" gate="G$1" x="101.6" y="-53.34"/>
<instance part="GND28" gate="1" x="93.98" y="-68.58"/>
<instance part="GND29" gate="1" x="101.6" y="-68.58"/>
<instance part="H3" gate="G$1" x="149.86" y="124.46" rot="R180"/>
<instance part="H4" gate="G$1" x="149.86" y="116.84" rot="R180"/>
<instance part="U$2" gate="G$1" x="160.02" y="119.38"/>
<instance part="U$3" gate="G$1" x="147.32" y="99.06"/>
<instance part="GND30" gate="1" x="160.02" y="99.06"/>
<instance part="FB1" gate="G$1" x="152.4" y="104.14"/>
<instance part="R34" gate="G$1" x="7.62" y="93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="6.1214" y="90.17" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="5.842" y="95.25" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY22" gate="G$1" x="7.62" y="99.06"/>
<instance part="SUPPLY24" gate="G$1" x="-10.16" y="40.64" rot="R180"/>
<instance part="SUPPLY26" gate="G$1" x="48.26" y="-93.98" rot="R180"/>
<instance part="J10" gate="G$1" x="-48.26" y="195.58" rot="R180"/>
<instance part="J11" gate="G$1" x="-48.26" y="182.88" rot="R180"/>
<instance part="J12" gate="G$1" x="-48.26" y="170.18" rot="R180"/>
<instance part="SUPPLY27" gate="G$1" x="-43.18" y="198.12"/>
<instance part="SUPPLY28" gate="G$1" x="-43.18" y="185.42"/>
<instance part="GND5" gate="1" x="-43.18" y="162.56"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="GND@11"/>
<wire x1="17.78" y1="66.04" x2="15.24" y2="66.04" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="15.24" y1="66.04" x2="15.24" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND@12"/>
<wire x1="15.24" y1="63.5" x2="15.24" y2="60.96" width="0.1524" layer="91"/>
<wire x1="17.78" y1="63.5" x2="15.24" y2="63.5" width="0.1524" layer="91"/>
<junction x="15.24" y="63.5"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND@13"/>
<wire x1="50.8" y1="63.5" x2="53.34" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="53.34" y1="63.5" x2="53.34" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="OE_N"/>
<wire x1="50.8" y1="86.36" x2="60.96" y2="86.36" width="0.1524" layer="91"/>
<wire x1="60.96" y1="86.36" x2="60.96" y2="88.9" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="60.96" y1="88.9" x2="63.5" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="99.06" y1="68.58" x2="99.06" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="91.44" y1="68.58" x2="91.44" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND23" gate="1" pin="GND"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="-17.78" y1="182.88" x2="-17.78" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND22" gate="1" pin="GND"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="182.88" x2="-27.94" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND@11"/>
<wire x1="-15.24" y1="-68.58" x2="-17.78" y2="-68.58" width="0.1524" layer="91"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="-17.78" y1="-68.58" x2="-17.78" y2="-71.12" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="GND@12"/>
<wire x1="-17.78" y1="-71.12" x2="-17.78" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-71.12" x2="-17.78" y2="-71.12" width="0.1524" layer="91"/>
<junction x="-17.78" y="-71.12"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND@13"/>
<wire x1="17.78" y1="-71.12" x2="20.32" y2="-71.12" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="20.32" y1="-71.12" x2="20.32" y2="-73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="OE_N"/>
<wire x1="17.78" y1="-48.26" x2="27.94" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-48.26" x2="27.94" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="27.94" y1="-45.72" x2="30.48" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="DIR"/>
<wire x1="-15.24" y1="-45.72" x2="-22.86" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-45.72" x2="-22.86" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="-22.86" y1="-43.18" x2="-25.4" y2="-43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="101.6" y1="-66.04" x2="101.6" y2="-63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND28" gate="1" pin="GND"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="93.98" y1="-66.04" x2="93.98" y2="-63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="FB1" gate="G$1" pin="2"/>
<wire x1="157.48" y1="104.14" x2="160.02" y2="104.14" width="0.1524" layer="91"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="160.02" y1="104.14" x2="160.02" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="-45.72" y1="167.64" x2="-43.18" y2="167.64" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="167.64" x2="-43.18" y2="165.1" width="0.1524" layer="91"/>
<pinref part="J12" gate="G$1" pin="1"/>
<wire x1="-45.72" y1="170.18" x2="-43.18" y2="170.18" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="170.18" x2="-43.18" y2="167.64" width="0.1524" layer="91"/>
<junction x="-43.18" y="167.64"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VCCB@23"/>
<wire x1="50.8" y1="88.9" x2="55.88" y2="88.9" width="0.1524" layer="91"/>
<pinref part="SUPPLY8" gate="G$1" pin="3.3V"/>
<wire x1="55.88" y1="88.9" x2="55.88" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VCCB@24"/>
<wire x1="55.88" y1="91.44" x2="55.88" y2="93.98" width="0.1524" layer="91"/>
<wire x1="50.8" y1="91.44" x2="55.88" y2="91.44" width="0.1524" layer="91"/>
<junction x="55.88" y="91.44"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="SUPPLY11" gate="G$1" pin="3.3V"/>
<wire x1="99.06" y1="78.74" x2="99.06" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="SUPPLY17" gate="G$1" pin="3.3V"/>
<wire x1="-17.78" y1="193.04" x2="-17.78" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="VCCB@23"/>
<wire x1="17.78" y1="-45.72" x2="22.86" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="SUPPLY3" gate="G$1" pin="3.3V"/>
<wire x1="22.86" y1="-45.72" x2="22.86" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VCCB@24"/>
<wire x1="22.86" y1="-43.18" x2="22.86" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-43.18" x2="22.86" y2="-43.18" width="0.1524" layer="91"/>
<junction x="22.86" y="-43.18"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="1"/>
<pinref part="SUPPLY20" gate="G$1" pin="3.3V"/>
<wire x1="101.6" y1="-55.88" x2="101.6" y2="-53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="68.58" y1="-86.36" x2="68.58" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-88.9" x2="63.5" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="63.5" y1="-88.9" x2="58.42" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-88.9" x2="53.34" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-88.9" x2="48.26" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-88.9" x2="43.18" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-88.9" x2="38.1" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-88.9" x2="33.02" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-88.9" x2="33.02" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="38.1" y1="-86.36" x2="38.1" y2="-88.9" width="0.1524" layer="91"/>
<junction x="38.1" y="-88.9"/>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="43.18" y1="-86.36" x2="43.18" y2="-88.9" width="0.1524" layer="91"/>
<junction x="43.18" y="-88.9"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="48.26" y1="-86.36" x2="48.26" y2="-88.9" width="0.1524" layer="91"/>
<junction x="48.26" y="-88.9"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="53.34" y1="-86.36" x2="53.34" y2="-88.9" width="0.1524" layer="91"/>
<junction x="53.34" y="-88.9"/>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="58.42" y1="-86.36" x2="58.42" y2="-88.9" width="0.1524" layer="91"/>
<junction x="58.42" y="-88.9"/>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="63.5" y1="-86.36" x2="63.5" y2="-88.9" width="0.1524" layer="91"/>
<junction x="63.5" y="-88.9"/>
<pinref part="SUPPLY26" gate="G$1" pin="3.3V"/>
<wire x1="48.26" y1="-93.98" x2="48.26" y2="-88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J11" gate="G$1" pin="2"/>
<pinref part="SUPPLY28" gate="G$1" pin="3.3V"/>
<wire x1="-45.72" y1="180.34" x2="-43.18" y2="180.34" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="180.34" x2="-43.18" y2="182.88" width="0.1524" layer="91"/>
<pinref part="J11" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="182.88" x2="-43.18" y2="185.42" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="182.88" x2="-43.18" y2="182.88" width="0.1524" layer="91"/>
<junction x="-43.18" y="182.88"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="SUPPLY9" gate="G$1" pin="5V"/>
<wire x1="12.7" y1="91.44" x2="12.7" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VCCA"/>
<wire x1="17.78" y1="91.44" x2="12.7" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<pinref part="SUPPLY10" gate="G$1" pin="5V"/>
<wire x1="91.44" y1="78.74" x2="91.44" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="1"/>
<pinref part="SUPPLY16" gate="G$1" pin="5V"/>
<wire x1="-27.94" y1="193.04" x2="-27.94" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY18" gate="G$1" pin="5V"/>
<wire x1="-20.32" y1="-43.18" x2="-20.32" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VCCA"/>
<wire x1="-15.24" y1="-43.18" x2="-20.32" y2="-43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="SUPPLY19" gate="G$1" pin="5V"/>
<wire x1="93.98" y1="-55.88" x2="93.98" y2="-53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R34" gate="G$1" pin="2"/>
<pinref part="SUPPLY22" gate="G$1" pin="5V"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="10.16" y1="48.26" x2="10.16" y2="45.72" width="0.1524" layer="91"/>
<wire x1="10.16" y1="45.72" x2="5.08" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="5.08" y1="45.72" x2="0" y2="45.72" width="0.1524" layer="91"/>
<wire x1="0" y1="45.72" x2="-5.08" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="45.72" x2="-10.16" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="45.72" x2="-15.24" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="45.72" x2="-20.32" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="45.72" x2="-25.4" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="45.72" x2="-25.4" y2="48.26" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="48.26" x2="-20.32" y2="45.72" width="0.1524" layer="91"/>
<junction x="-20.32" y="45.72"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="48.26" x2="-15.24" y2="45.72" width="0.1524" layer="91"/>
<junction x="-15.24" y="45.72"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="48.26" x2="-10.16" y2="45.72" width="0.1524" layer="91"/>
<junction x="-10.16" y="45.72"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="48.26" x2="-5.08" y2="45.72" width="0.1524" layer="91"/>
<junction x="-5.08" y="45.72"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="0" y1="48.26" x2="0" y2="45.72" width="0.1524" layer="91"/>
<junction x="0" y="45.72"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="5.08" y1="48.26" x2="5.08" y2="45.72" width="0.1524" layer="91"/>
<junction x="5.08" y="45.72"/>
<pinref part="SUPPLY24" gate="G$1" pin="5V"/>
<wire x1="-10.16" y1="45.72" x2="-10.16" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="1"/>
<pinref part="SUPPLY27" gate="G$1" pin="5V"/>
<wire x1="-45.72" y1="195.58" x2="-43.18" y2="195.58" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="195.58" x2="-43.18" y2="198.12" width="0.1524" layer="91"/>
<pinref part="J10" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="193.04" x2="-43.18" y2="193.04" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="193.04" x2="-43.18" y2="195.58" width="0.1524" layer="91"/>
<junction x="-43.18" y="195.58"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A1"/>
<wire x1="-30.48" y1="86.36" x2="-25.4" y2="86.36" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="1"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="-25.4" y1="86.36" x2="17.78" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="58.42" x2="-25.4" y2="86.36" width="0.1524" layer="91"/>
<junction x="-25.4" y="86.36"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A2"/>
<wire x1="-30.48" y1="83.82" x2="-20.32" y2="83.82" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="2"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="-20.32" y1="83.82" x2="17.78" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="58.42" x2="-20.32" y2="83.82" width="0.1524" layer="91"/>
<junction x="-20.32" y="83.82"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A3"/>
<wire x1="-30.48" y1="81.28" x2="-15.24" y2="81.28" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="3"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="-15.24" y1="81.28" x2="17.78" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="58.42" x2="-15.24" y2="81.28" width="0.1524" layer="91"/>
<junction x="-15.24" y="81.28"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A4"/>
<wire x1="-30.48" y1="78.74" x2="-10.16" y2="78.74" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="4"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="78.74" x2="17.78" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="58.42" x2="-10.16" y2="78.74" width="0.1524" layer="91"/>
<junction x="-10.16" y="78.74"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A5"/>
<wire x1="-30.48" y1="76.2" x2="-5.08" y2="76.2" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="5"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="-5.08" y1="76.2" x2="17.78" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="58.42" x2="-5.08" y2="76.2" width="0.1524" layer="91"/>
<junction x="-5.08" y="76.2"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A6"/>
<pinref part="J4" gate="G$1" pin="6"/>
<wire x1="17.78" y1="73.66" x2="0" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="0" y1="73.66" x2="-30.48" y2="73.66" width="0.1524" layer="91"/>
<wire x1="0" y1="58.42" x2="0" y2="73.66" width="0.1524" layer="91"/>
<junction x="0" y="73.66"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A7"/>
<pinref part="J4" gate="G$1" pin="7"/>
<wire x1="17.78" y1="71.12" x2="5.08" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="5.08" y1="71.12" x2="-30.48" y2="71.12" width="0.1524" layer="91"/>
<wire x1="5.08" y1="58.42" x2="5.08" y2="71.12" width="0.1524" layer="91"/>
<junction x="5.08" y="71.12"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A8"/>
<pinref part="J4" gate="G$1" pin="8"/>
<wire x1="17.78" y1="68.58" x2="10.16" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="10.16" y1="68.58" x2="-30.48" y2="68.58" width="0.1524" layer="91"/>
<wire x1="10.16" y1="58.42" x2="10.16" y2="68.58" width="0.1524" layer="91"/>
<junction x="10.16" y="68.58"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="B1"/>
<pinref part="J5" gate="G$1" pin="8"/>
<wire x1="50.8" y1="83.82" x2="71.12" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="B2"/>
<pinref part="J5" gate="G$1" pin="7"/>
<wire x1="50.8" y1="81.28" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="B3"/>
<pinref part="J5" gate="G$1" pin="6"/>
<wire x1="50.8" y1="78.74" x2="71.12" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="B4"/>
<pinref part="J5" gate="G$1" pin="5"/>
<wire x1="50.8" y1="76.2" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="B5"/>
<pinref part="J5" gate="G$1" pin="4"/>
<wire x1="50.8" y1="73.66" x2="71.12" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="B6"/>
<pinref part="J5" gate="G$1" pin="3"/>
<wire x1="50.8" y1="71.12" x2="71.12" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="B7"/>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="50.8" y1="68.58" x2="71.12" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="B8"/>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="50.8" y1="66.04" x2="71.12" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="A1"/>
<pinref part="J8" gate="G$1" pin="1"/>
<wire x1="-30.48" y1="-48.26" x2="-15.24" y2="-48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="A2"/>
<pinref part="J8" gate="G$1" pin="2"/>
<wire x1="-30.48" y1="-50.8" x2="-15.24" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="A3"/>
<pinref part="J8" gate="G$1" pin="3"/>
<wire x1="-30.48" y1="-53.34" x2="-15.24" y2="-53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="A4"/>
<pinref part="J8" gate="G$1" pin="4"/>
<wire x1="-30.48" y1="-55.88" x2="-15.24" y2="-55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="A5"/>
<wire x1="-30.48" y1="-58.42" x2="-15.24" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="A6"/>
<pinref part="J8" gate="G$1" pin="6"/>
<wire x1="-15.24" y1="-60.96" x2="-30.48" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="A7"/>
<pinref part="J8" gate="G$1" pin="7"/>
<wire x1="-15.24" y1="-63.5" x2="-30.48" y2="-63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="A8"/>
<pinref part="J8" gate="G$1" pin="8"/>
<wire x1="-15.24" y1="-66.04" x2="-30.48" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="B1"/>
<pinref part="J9" gate="G$1" pin="8"/>
<wire x1="17.78" y1="-50.8" x2="68.58" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="68.58" y1="-50.8" x2="76.2" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-76.2" x2="68.58" y2="-50.8" width="0.1524" layer="91"/>
<junction x="68.58" y="-50.8"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="B2"/>
<pinref part="J9" gate="G$1" pin="7"/>
<wire x1="17.78" y1="-53.34" x2="63.5" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="63.5" y1="-53.34" x2="76.2" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-76.2" x2="63.5" y2="-53.34" width="0.1524" layer="91"/>
<junction x="63.5" y="-53.34"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="B3"/>
<pinref part="J9" gate="G$1" pin="6"/>
<wire x1="17.78" y1="-55.88" x2="58.42" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="58.42" y1="-55.88" x2="76.2" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-76.2" x2="58.42" y2="-55.88" width="0.1524" layer="91"/>
<junction x="58.42" y="-55.88"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="B4"/>
<pinref part="J9" gate="G$1" pin="5"/>
<wire x1="17.78" y1="-58.42" x2="53.34" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="53.34" y1="-58.42" x2="76.2" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-76.2" x2="53.34" y2="-58.42" width="0.1524" layer="91"/>
<junction x="53.34" y="-58.42"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="B5"/>
<pinref part="J9" gate="G$1" pin="4"/>
<wire x1="17.78" y1="-60.96" x2="48.26" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="48.26" y1="-60.96" x2="76.2" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-76.2" x2="48.26" y2="-60.96" width="0.1524" layer="91"/>
<junction x="48.26" y="-60.96"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="B6"/>
<pinref part="J9" gate="G$1" pin="3"/>
<wire x1="17.78" y1="-63.5" x2="43.18" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="43.18" y1="-63.5" x2="76.2" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-76.2" x2="43.18" y2="-63.5" width="0.1524" layer="91"/>
<junction x="43.18" y="-63.5"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="B7"/>
<pinref part="J9" gate="G$1" pin="2"/>
<wire x1="17.78" y1="-66.04" x2="38.1" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="38.1" y1="-66.04" x2="76.2" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-76.2" x2="38.1" y2="-66.04" width="0.1524" layer="91"/>
<junction x="38.1" y="-66.04"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="B8"/>
<pinref part="J9" gate="G$1" pin="1"/>
<wire x1="17.78" y1="-68.58" x2="33.02" y2="-68.58" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="33.02" y1="-68.58" x2="76.2" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-76.2" x2="33.02" y2="-68.58" width="0.1524" layer="91"/>
<junction x="33.02" y="-68.58"/>
</segment>
</net>
<net name="E_GND" class="0">
<segment>
<pinref part="H4" gate="G$1" pin="MOUNT"/>
<wire x1="152.4" y1="116.84" x2="154.94" y2="116.84" width="0.1524" layer="91"/>
<wire x1="154.94" y1="116.84" x2="154.94" y2="121.92" width="0.1524" layer="91"/>
<pinref part="H3" gate="G$1" pin="MOUNT"/>
<wire x1="154.94" y1="121.92" x2="154.94" y2="124.46" width="0.1524" layer="91"/>
<wire x1="154.94" y1="124.46" x2="152.4" y2="124.46" width="0.1524" layer="91"/>
<wire x1="154.94" y1="121.92" x2="160.02" y2="121.92" width="0.1524" layer="91"/>
<junction x="154.94" y="121.92"/>
<pinref part="U$2" gate="G$1" pin="E_GND"/>
<wire x1="160.02" y1="121.92" x2="160.02" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="E_GND"/>
<wire x1="147.32" y1="99.06" x2="147.32" y2="104.14" width="0.1524" layer="91"/>
<pinref part="FB1" gate="G$1" pin="1"/>
<wire x1="147.32" y1="104.14" x2="149.86" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="DIR"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="17.78" y1="88.9" x2="7.62" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
