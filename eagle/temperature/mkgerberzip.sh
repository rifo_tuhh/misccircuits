#!/bin/sh
mv -f temperatur.GBL \
      temperatur.GBO \
      temperatur.GBP \
      temperatur.GBS \
      temperatur.GML \
      temperatur.GTL \
      temperatur.GTO \
      temperatur.GTP \
      temperatur.GTS \
      temperatur.TXT \
      gerbers
rm    temperatur.gpi temperatur.dri
rm -f temperatur-gerbers.zip
zip -r temperatur-gerbers.zip gerbers/
