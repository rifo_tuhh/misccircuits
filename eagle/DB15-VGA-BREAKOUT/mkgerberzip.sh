#!/bin/sh
mv -f DB15-VGA-BREAKOUT.GBL \
      DB15-VGA-BREAKOUT.GBO \
      DB15-VGA-BREAKOUT.GBP \
      DB15-VGA-BREAKOUT.GBS \
      DB15-VGA-BREAKOUT.GML \
      DB15-VGA-BREAKOUT.GTL \
      DB15-VGA-BREAKOUT.GTO \
      DB15-VGA-BREAKOUT.GTP \
      DB15-VGA-BREAKOUT.GTS \
      DB15-VGA-BREAKOUT.TXT \
      gerbers
rm    DB15-VGA-BREAKOUT.gpi DB15-VGA-BREAKOUT.dri
rm -f db15-vga-gerbers.zip
zip -r db15-vga-gerbers.zip gerbers/
