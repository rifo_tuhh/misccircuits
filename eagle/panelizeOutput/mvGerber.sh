#!/bin/sh

mv -f placement.panelized.txt  \
      panelized.GBO \
      panelized.GTO \
      panelized.GBS \
      panelized.GTL \
      panelized.GBL \
      panelized.oln \
      panelized.GTS \
      panelized.bor \
      panelized.sco \
      toollist.panelized.drl \
      panelized.TXT \
      panelized.fab \
      gerbers
zip -r panelized_gerbers.zip gerbers/
