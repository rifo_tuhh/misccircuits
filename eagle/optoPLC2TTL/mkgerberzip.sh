#!/bin/sh
mv -f optoPLC2TTL.GBL \
      optoPLC2TTL.GBO \
      optoPLC2TTL.GBP \
      optoPLC2TTL.GBS \
      optoPLC2TTL.GML \
      optoPLC2TTL.gpi \
      optoPLC2TTL.dri \
      optoPLC2TTL.GTL \
      optoPLC2TTL.GTO \
      optoPLC2TTL.GTP \
      optoPLC2TTL.GTS \
      optoPLC2TTL.TXT \
      gerbers
rm    optoPLC2TTL.gpi optoPLC2TTL.dri
rm -f optoPLC2TTL-gerbers.zip
zip -r optoPLC2TTL-gerbers.zip gerbers/
