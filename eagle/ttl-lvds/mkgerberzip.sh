#!/bin/sh
mv -f ttl-lvds.GBL \
      ttl-lvds.GBO \
      ttl-lvds.GBP \
      ttl-lvds.GBS \
      ttl-lvds.GML \
      ttl-lvds.GTL \
      ttl-lvds.GTO \
      ttl-lvds.GTP \
      ttl-lvds.GTS \
      ttl-lvds.TXT \
      gerbers
rm ttl-lvds.gpi ttl-lvds.dri
rm -f ttl-lvds-gerbers.zip
zip -r ttl-lvds-gerbers.zip gerbers/
